# Generated by Django 4.0.4 on 2023-02-25 23:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_alter_ciudad_options_alter_prioridad_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ciudad',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='colaservicio',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='empleado',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='prioridad',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='tipoempleado',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='tipopersona',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
        migrations.AlterField(
            model_name='tiposervicio',
            name='usuario_insercion',
            field=models.CharField(blank=True, max_length=16, null=True),
        ),
    ]
