# Generated by Django 4.0.4 on 2023-02-23 01:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_alter_ticket_numero_orden_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='colaservicio',
            name='numero_orden',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='colaservicio',
            name='persona',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app.persona'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='ticket',
            name='numero_orden',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
