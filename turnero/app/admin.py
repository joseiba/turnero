from django.contrib import admin
from app.models import (Ciudad, Cliente, ColaServicio, Empleado, Permiso, Persona, Prioridad,
Rol, RolPermiso, Servicio, Ticket, TipoEmpleado, TipoPersona, TipoServicio, Usuario)

# Register your models here.
admin.site.register([Ciudad, Cliente, Empleado, Permiso, Persona, Prioridad,
Rol, RolPermiso, Servicio, Ticket, ColaServicio, TipoEmpleado, TipoPersona, TipoServicio, Usuario])
