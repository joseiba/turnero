from django import forms

from app.models import Cliente, Persona, Ticket

class PersonaForm(forms.ModelForm):
    """[summary]
    Args:
        forms ([PersonaForm]): [Formulario de Persona]
    """    
    class Meta:
        model = Persona
        exclude = ['activo']
        widgets = {
			'nombre' : forms.TextInput(attrs={'class':'form-control','autocomplete': 'off',
                'name': 'nombre', 'placeholder': 'Nombre del Cliente', 'required': 'required','onkeyup':'replaceCaratect(this)'}),
			'apellido' : forms.TextInput(attrs={'class':'form-control','autocomplete': 'off', 'name': 'apellido_cliente', 
                'placeholder': 'Apellido del Cliente', 'required': 'required','onkeyup':'replaceCaratect(this)'}),
			'direccion' : forms.TextInput(attrs={'class':'form-control','name': 'direccion', 'placeholder': 'Dirección',
                'onkeyup':'replaceDirection(this)','type':'text', 'required': 'required', 'autocomplete': 'off'}),
			'numero_documento' : forms.TextInput(attrs={'class':'form-control', 'name':'numero_documento', 'placeholder': 'Nro. Cédula', 
                'required':'required','onkeyup':'replaceABC(this)', 'autocomplete': 'off'}),
			'telefono' : forms.TextInput(attrs={'class':'form-control tel','placeholder': 'Telefono','type': 'text', 
            'name':'telefono', 'required':'required','onkeyup':'replaceABC(this)', 'autocomplete': 'off'}),
            'email' : forms.TextInput(attrs={'class':'form-control optional', 'placeholder': 'Email','name':'email', 
                'type':'text', 'id':'email', 'autocomplete': 'off'}),
            'fecha_nacimiento' : forms.TextInput(attrs={'class':'form-control', 'type': 'text',
                'name':'fecha_nacimiento','autocomplete': 'off', 'placeholder': 'Fecha Nacimiento'}),
            'ciudad' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'ciudad','required':'required' ,'name':'ciudad',
            'autocomplete': 'off', 'placeholder': 'ciudad'}),
            'sexo' : forms.Select(attrs={'class':'selectpicker',  'data-live-search': 'true', 'id': 'sexo','required':'required' ,'name':'sexo', 'autocomplete': 'off'}),
            'prioridad' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'prioridad','required':'required' ,'name':'prioridad', 'autocomplete': 'off'}),
            'servicio' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'servicio','required':'required' ,'name':'servicio', 'autocomplete': 'off'}),
		}


class TicketForm(forms.ModelForm):
    """[summary]
    Args:
        forms ([TicketForm]): [Formulario de Ticket]
    """    
    class Meta:
        model = Ticket
        exclude = ['activo']
        widgets = {			
            'persona' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'persona',
                'required':'required' ,'name':'persona','autocomplete': 'off', 'placeholder': 'persona'}),
            'prioridad' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'prioridad',
                'required':'required' ,'name':'prioridad', 'autocomplete': 'off'}),
            'servicio' : forms.Select(attrs={'class':'selectpicker', 'data-live-search': 'true', 'id': 'servicio',
             'required':'required' ,'name':'servicio', 'autocomplete': 'off'}),
		}