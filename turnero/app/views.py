import math
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from queue import PriorityQueue
from django.db.models import Q
from django.core.paginator import Paginator



from .forms import PersonaForm, TicketForm
from .models import Ciudad, Persona, Ticket, ColaServicio, Servicio, Prioridad


# Create your views here.
def index(request):
    servicio = Servicio.objects.all()
    context = {'servicio': servicio} 
    return render(request, "home/index.html", context)

#clientes
def list_clientes(request):    
    return render(request, "cliente/lista.html")

def add_cliente(request):
    form = PersonaForm
    if request.method == 'POST':
        form = PersonaForm(request.POST or None)
        if form.is_valid():           
            messages.success(request, 'Se ha agregado correctamente!')
            form.save()
            return redirect('/cliente/agregar')
    cuidad = Ciudad.objects.all()   
    context = {'form' : form, 'cuidad' : cuidad}
    return render(request, 'cliente/agregar.html', context)

def edit_cliente(request, id):
    cliente = Persona.objects.get(id=id)
    form = PersonaForm(instance=cliente)
    if request.method == 'POST':
        form = PersonaForm(request.POST, instance=cliente)
        if not form.has_changed():
            messages.info(request, "No has hecho ningun cambio!")
            return redirect('/cliente/editar/' + str(id))
        if form.is_valid():
            cliente = form.save(commit=False)
            cliente.save()          
            messages.success(request, 'Se ha editado correctamente!')
            return redirect('/cliente/editar/' + str(id))
    
    context = {'form': form, 'cliente': cliente}
    return render(request, 'cliente/editar.html', context)



def list_client_ajax(request):
    query = request.GET.get('busqueda')
    clientes = Persona.objects.order_by('-fecha_insercion')

    total = clientes.count()

    _start = request.GET.get('start')
    _length = request.GET.get('length')
    if _start and _length:
        start = int(_start)
        length = int(_length)
        page = math.ceil(start / length) + 1
        per_page = length

        clientes = clientes[start:start + length]

    data = [{'id': clie.pk, 'nombre': clie.nombre, 'apellido': clie.apellido, 
        'cedula': clie.numero_documento, 'telefono': clie.telefono, 'direccion': clie.direccion, 
            'ciudad': clie.ciudad.nombre_ciudad } 
        for clie in clientes]        
        
    response = {
        'data': data,
        'recordsTotal': total,
        'recordsFiltered': total,
    }
    return JsonResponse(response)


 #Tikects
def add_ticket(request):
    form = TicketForm
    if request.method == 'POST':
        form = TicketForm(request.POST or None)
        if form.is_valid():
            if registrar_prioridad(request):
                messages.success(request, 'Se ha agregado correctamente!')
                form.save()
                return redirect('/ticket/agregar')
            else:
                messages.warning(request, 'Ya no hay mas turnos para este servicio!')
                return redirect('/ticket/agregar')
    context = {'form' : form}
    return render(request, 'ticket/agregar.html', context)


def ver_cola_servicio(request):  
    servicio = Servicio.objects.all()
    context = {'servicio': servicio} 
    return render(request, "ticket/lista.html",context)

def mostrar(request, id):  
    cola = ColaServicio.objects.get(pk=id)
    context = {'cola': cola}
    return render(request, "ticket/mostrar.html", context)

def eliminar(request, id):
    cola = ColaServicio.objects.get(pk=id)
    cola.delete()
    messages.success(request, 'Se ha eliminado de la cola correctamente!')
    return redirect('/ticket/lista')

def get_cola(request):
    query = request.GET.get('busqueda')
    if query != '':
        cola =  ColaServicio.objects.filter(Q(servicio__pk__icontains=query)).order_by('prioridad__codigo_prioridad')
    else:
        cola =  ColaServicio.objects.filter(Q(servicio__pk__icontains=0)).order_by('prioridad__codigo_prioridad')

    total = cola.count()

    _start = request.GET.get('start')
    _length = request.GET.get('length')
    if _start and _length:
        start = int(_start)
        length = int(_length)
        page = math.ceil(start / length) + 1
        per_page = length

        cola = cola[start:start + length]

    data = [{'id': c.pk, 'nombre': c.persona.nombre, 'apellido': c.persona.apellido, 
            'cedula': c.persona.numero_documento, 'servicio': c.servicio.nombre_servicio,
            'prioridad': c.prioridad.condicion } for c in cola]        

    response = {
        'data': data,
        'recordsTotal': total,
        'recordsFiltered': total,
    }
    return JsonResponse(response) 

    

def registrar_prioridad(request):
    try:
        cola = ColaServicio()
        q = PriorityQueue()
        id_servicio = request.POST.get('servicio') 
        id_persona = request.POST.get('persona') 
        cola_servicio = ColaServicio.objects.filter(servicio=id_servicio)
        persona = Persona.objects.get(id=id_persona)
        servicio = Servicio.objects.get(id=id_servicio)
        prioridad = Prioridad.objects.get(id=request.POST.get('prioridad'))
        if  cola_servicio.count() == servicio.cantidad_cola:
            return False       
        else:          
            cola.usuario_insercion ="jose"
            cola.numero_orden= 1
            cola.servicio=servicio
            cola.persona=persona
            cola.prioridad = prioridad          
            cola.save()            
            return True
    except Exception as e:
        return False


