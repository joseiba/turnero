# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import AbstractUser



class Ciudad(models.Model):
    nombre_ciudad = models.CharField(max_length=100)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    activo = models.BooleanField(default=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self):
        """Formato de la ciudad"""
        return '{0}'.format(self.nombre_ciudad)
    class Meta:
        verbose_name = "Ciudad"
        verbose_name_plural = "Ciudades"


class Cliente(models.Model):
    activo = models.BooleanField(default=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE)



class ColaServicio(models.Model):
    activo = models.BooleanField(default=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    numero_orden = models.IntegerField(blank=True, null=True)
    servicio = models.ForeignKey('Servicio', on_delete=models.CASCADE)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE) 
    prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE)  
    def __str__(self):
        """Formato de la ColaServicio"""
        return '{0}'.format(self.servicio.nombre_servicio)

class Empleado(models.Model):
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    usuario = models.CharField(max_length=25)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    password = models.CharField(max_length=50)
    activo = models.BooleanField(default=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)


class Permiso(models.Model):
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField(default=True, blank=True)


class Persona(models.Model):
    MASCULINO = 'MAS'
    FEMENINO = 'FEM'    
    tipo_sexo =((MASCULINO, 'Masculino'),
                (FEMENINO, 'Femenino'))
    numero_documento = models.CharField(max_length=16)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64)
    fecha_nacimiento = models.DateField()
    sexo = models.CharField(max_length=15, choices=tipo_sexo, default="-", help_text="Seleccione el sexo")
    direccion = models.CharField(max_length=70, blank=True, null=True)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    activo = models.BooleanField(default=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    ciudad = models.ForeignKey('Ciudad', on_delete=models.CASCADE)

    
    def __str__(self):
        """Formato de la Persona"""
        return '{0}'.format(self.nombre)

class Prioridad(models.Model):
    codigo_prioridad = models.IntegerField()
    tipo_prioridad = models.CharField(max_length=1)
    condicion = models.CharField(max_length=200)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    activo = models.BooleanField(default=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    
    def __str__(self):
        """Formato de la Prioridad"""
        return '{0}'.format(self.condicion)

    class Meta:
        verbose_name = "Prioridad"
        verbose_name_plural = "Prioridades"


class Rol(models.Model):
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField(default=True, blank=True)



class RolPermiso(models.Model):
    permiso = models.ForeignKey(Permiso, on_delete=models.CASCADE)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)


class Servicio(models.Model):
    nombre_servicio = models.CharField(max_length=200)
    activo = models.BooleanField(default=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    cantidad_cola = models.IntegerField()

    def __str__(self):
        """Formato del Servicio"""
        return '{0}'.format(self.nombre_servicio)


class Ticket(models.Model):
    numero_orden = models.IntegerField(blank=True, null=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True)
    activo = models.BooleanField(default=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    servicio = models.ForeignKey('Servicio', on_delete=models.CASCADE)
    prioridad = models.ForeignKey('Prioridad', on_delete=models.CASCADE)
    persona = models.ForeignKey('Persona', on_delete=models.CASCADE)
    def __str__(self):
        """Formato de la Ticket"""
        return '{0}'.format(self.servicio.nombre_servicio)

class TipoEmpleado(models.Model):
    nombre_tipo_empleado = models.CharField(max_length=200)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    activo = models.BooleanField(default=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)


class TipoPersona(models.Model):
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField(default=True, blank=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)


class TipoServicio(models.Model):
    nombre_tipo_servicio = models.CharField(max_length=200)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)
    fecha_modificacion = models.DateTimeField(auto_now=True, blank=True)
    activo = models.BooleanField(default=True, blank=True)
    usuario_insercion = models.CharField(max_length=16, blank=True, null=True)
    fecha_insercion = models.DateTimeField(auto_now=True, blank=True)


class Usuario(AbstractUser):
    nombre = models.CharField(max_length=16)

