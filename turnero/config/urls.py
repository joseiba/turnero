"""turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from app.views import (index, list_clientes, add_cliente, add_ticket, ver_cola_servicio, 
get_cola, mostrar, list_client_ajax, edit_cliente, eliminar)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name="index"),

    #clientes
    path('cliente/lista', list_clientes, name="list_clientes"),
    path('cliente/agregar', add_cliente, name="add_cliente"),
    path('cliente/get_list_client/', list_client_ajax, name="list_client_ajax"),
    path('cliente/editar/<int:id>/', edit_cliente, name="edit_cliente"),

    #ticket
    path('ticket/lista', ver_cola_servicio, name="ver_cola_servicio"),
    path('ticket/agregar', add_ticket, name="add_ticket"),
    path('cola/get_cola/', get_cola, name="get_cola"),
    path('tickect/mostrar/<int:id>/', mostrar, name="mostrar"),
    path('cola/eliminar/<int:id>/', eliminar, name="eliminar"),
]
