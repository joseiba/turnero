# turnero



## Getting started

Esta aplicacion web, es un ejemplo de un turnero simple.

## Clonar el Repositorio

- Debe clonar primeramente el proyecto: 

```
git clone https://gitlab.com/joseiba/turnero.git
```

## Instalacion 

Para instalar los requerimientos primero debe tener python instalado en su equipo



Luego de instalarlo, debe ejecutar el siguiente comando para instalar los requerimientos:

```
pip install -r requirements.txt
```
Debe tener creador una base de datos con el **"turnero"** en PosgresSQL

Hay 2 formas de levantar el proyecto:

**La primera es restaurar un backup de la base de datos en el PosgresSQL para no volver a cargar todas las configuraciones**

El archivo del backup esta en el proyecto

El user que esta en el backup para acceder al admin de django es:

**User**: jose

**Pass**: jose

O se puede crear un nuevo super usuario con el siguiente comando:

```
python manage.py createsuperuser
```
Luego ya puede levantar el servido con el comando:

```
python manage.py runserver
```

**La segunda forma es la siguiente:**

Debe abrir la linea de comando o dentro del vsCode, en la terminal, acceder dentro de la carpeta turnero donde contiene 
el archico manage.python

Ejecutar primero el comando:

```
python manage.py makemigrations
```

Despues el comando:

```
python manage.py migrate
```
Debe crear un super usuario con el siguiente comando:

```
python manage.py createsuperuser
```

Luego ya puede levantar el servido con el comando:

```
python manage.py runserver
```
